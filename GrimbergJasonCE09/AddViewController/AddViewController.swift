//
//  AddViewController.swift
//  GrimbergJasonCE09
//
//  Created by Jason Grimberg on 9/21/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import UIKit

class AddViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    // Set the original text
    var resetText = "battlestations"
    // Variables to hold the subs
    var addedSubs = [""]
    
    @IBOutlet weak var subredditText: UITextField!
    @IBOutlet weak var addTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Reload the table view
        addTableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // MARK: - UITableViewDataSource Protocol Callbacks
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addedSubs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Reuse an existing cell or create a new one if needed
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell_ID_1", for: indexPath)
        
        // Configure cell
        cell.textLabel?.text = addedSubs[indexPath.row]
        
        // Return our configured cell
        return cell
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
 
    // MARK: - All actions
    
    @IBAction func addTapped(_ sender: UIButton) {
        
        if subredditText.text != "" {
            let newSub = subredditText.text
            // Append the new text
            addedSubs.append(newSub!)
            // Clear the text
            subredditText.text = ""
        }
        
        // Reload the table view
        addTableView.reloadData()
    }
    
    @IBAction func resetTapped(_ sender: UIBarButtonItem) {
        // Remove everything from the array
        addedSubs.removeAll()
        // Add the original back to the array
        addedSubs.append(resetText)
        // Reload the table view
        addTableView.reloadData()
        
    }
    
    @IBAction func saveTapped(_ sender: UIBarButtonItem) {
        
        // Save the current tableView via our extension method
        UserDefaults.standard.set(tableContents: addedSubs, forKey: "tableViewSave")
        
        // Clear the textField
        subredditText.text = ""
        
        // Reload the table view
        addTableView.reloadData()
        
        print("Saved!")
    }
    
}
