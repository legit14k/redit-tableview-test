//
//  ViewController.swift
//  GrimbergJasonCE09
//
//  Created by Jason Grimberg on 9/20/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var filteredPosts = [[RedditInfo]]()
    
    // Set defaults for first time use
    var redditTitle = ["battlestations"]
    var redditExtension = ".json"
    var titleTextColor = UIColor.black
    var detailTextColor = UIColor.black
    var cellBackGroundColor = UIColor.white
    
    @IBOutlet weak var tableView: UITableView!
    
    // Create an empty array of Reddit data to fill out
    var redditInfo = [RedditInfo]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Call the user defaults
        callBackColors()
        
        for i in redditTitle {
            // This did not have any 'no thumbnails' in it
            downloadAndParse(jsonAtUrl: "https://www.reddit.com/r/\(i)/\(redditExtension)")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return filteredPosts.count
    }
    
    // MARK: - UITableViewDataSource Protocol Callbacks
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredPosts[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Reuse an existing cell or create a new one if needed
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell_ID_1", for: indexPath) as! MainTableViewCell
        
        let currentPost = filteredPosts[indexPath.section][indexPath.row]
        
        // Configure cell
        // Configure the Color
        cell.titleLabel?.textColor = titleTextColor
        cell.authorLabel?.textColor = detailTextColor
        cell.contentView.backgroundColor = cellBackGroundColor
        
        // Set the title
        cell.titleLabel?.text = currentPost.title
        // Set the name
        cell.authorLabel?.text = currentPost.name
        // Set the thumbnail if there is one
        cell.thumbnail?.image = currentPost.thumbNail
        
        // Return our configured cell
        return cell
    }
    
    // MARK: - Header Methods
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return filteredPosts[section].first?.sub
    }
    
    // Unwind from the details VC and update the data from the source
    @IBAction func unwindFromTheme(_ sender: UIStoryboardSegue) {
        if sender.source is ThemeViewController {
            if let senderVC = sender.source as? ThemeViewController {
                titleTextColor = senderVC.titleLabel.textColor
                detailTextColor = senderVC.authorLabel.textColor
                cellBackGroundColor = senderVC.preView.backgroundColor!
            }
            // Reload the table view
            tableView.reloadData()
        }
        
        if sender.source is AddViewController {
            if let senderVC = sender.source as? AddViewController {
                redditTitle += senderVC.addedSubs
            }
            // Reload the table view
            tableView.reloadData()
        }
    }
    
    func callBackColors() {
        // Load color from UserDefaults
        if let viewColorDefault = UserDefaults.standard.color(forKey: "viewColor"){
            cellBackGroundColor = viewColorDefault
        }
        if let titleColorDefault = UserDefaults.standard.color(forKey: "titleColor"){
            titleTextColor = titleColorDefault
        }
        if let authorColorDefault = UserDefaults.standard.color(forKey: "authorColor"){
            detailTextColor = authorColorDefault
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let postToSend = redditTitle
        if let destination = segue.destination as? AddViewController {
            destination.addedSubs = postToSend
        }
    }
}

