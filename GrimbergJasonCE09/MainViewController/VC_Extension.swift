//
//  VC_Extension.swift
//

import Foundation
import UIKit

extension ViewController {

    //Helper method to download and parse json at a url(String)
    func downloadAndParse(jsonAtUrl urlString: String) {

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        if let validURL = URL(string: urlString) {

            let task = session.dataTask(with: validURL, completionHandler: { (opt_data, opt_response, opt_error) in

                // Bail Out on error
                if opt_error != nil { return }

                // Check the response, statusCode, and data
                guard let response = opt_response as? HTTPURLResponse,
                    response.statusCode == 200,
                    let data = opt_data
                    else { return }

                var tempString = ""
                do {
                    // De-Serialize data object
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                        // Parse Data
                        guard let mainData = json["data"] as? [String: Any],
                            let child = mainData["children"] as? [[String: Any]]
                            else { return }
                        
                        // Check if there is data to display
                        if child.count > 0 {
                            for i in child {
                                guard let data = i["data"] as? [String: Any],
                                    let sub = data["subreddit"] as? String,
                                    let name = data["author_fullname"] as? String,
                                    let title = data["title"] as? String,
                                    let thumbnail = data["thumbnail"] as? String
                                    else { continue }
                                
                                // Check to see if the thumbnail starts with h as in https
                                if thumbnail.description.first == "h"{
                                    tempString = sub
                                    // Setting the current reddit info to a custom model
                                    let currentInfo = (RedditInfo(sub: sub, name: name, title: title, thumbnailString: thumbnail))
                                    // Append to redditInfo Class
                                    self.redditInfo.append(currentInfo)
                                } else {
                                    // Show that the thumbnail was not found and skip over
                                    print("Thumbnail not found")
                                }
                            }
                        }
                    }
                }
                catch {
                    // Catch any errors that come through
                    print(error.localizedDescription)
                }
                
                // Make sure you reload the data after it has been downloaded
                DispatchQueue.main.async {
                    let redditInfo2 = self.redditInfo
                    self.filteredPosts.append(redditInfo2.filter({ $0.sub == tempString }))
                    //self.filterPostsBySubreddit()
                    self.tableView.reloadData()
                }
            })
            // Resume
            task.resume()
        }
    }
    
//    // Filter through each sub-reddit
//    func filterPostsBySubreddit() {
//        let redditInfo2 = redditInfo
//        let redditInfo3 = redditInfo2
//
//        filteredPosts.append(redditInfo2.filter({ $0.sub == "battlestations"}))
//        filteredPosts.append(redditInfo3.filter({ $0.sub == "puppies"}))
////        filteredPosts[0] = posts.filter({ $0.party == "R" })
////        filteredPosts[1] = posts.filter({ $0.party == "D" })
////        filteredPosts[2] = posts.filter({ $0.party == "I" })
//    }
}
