//
//  ReditInfo.swift
//  GrimbergJasonCE04
//
//  Created by Jason Grimberg on 9/7/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import Foundation
import UIKit

class RedditInfo {
    
    /* Stored Properties */
    var sub: String
    var name: String
    var title: String
    var thumbNail: UIImage!
    
    /* Initializers */
    init(sub: String, name: String, title: String, thumbnailString: String) {
        
        // Set author and title
        self.sub = sub
        self.name = "Author: \(name)"
        self.title = title
        
        // Check to make sure if the thumbnail can return and download
        if let url = URL(string: thumbnailString) {
            
            do {
                let data = try Data(contentsOf: url)
                self.thumbNail = UIImage(data: data)
            } catch {
                print(error.localizedDescription)
            }
        }
    }
}
