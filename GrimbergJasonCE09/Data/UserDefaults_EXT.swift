//
//  UserDefaults_EXT.swift
//  GrimbergJasonCE09
//
//  Created by Jason Grimberg on 10/17/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import Foundation
import UIKit

extension UserDefaults {
    
    // Save a tableView as a data object
    func set(tableContents: [String], forKey key: String) {
        
        // Convert the UITableView object into a Data Object by Archiving
        let binaryData = NSKeyedArchiver.archivedData(withRootObject: tableContents)
        
        // Save Binary Data to UserDefaults
        self.set(binaryData, forKey: key)
    }
    
    // Get UITableView from the saved Defaults with a key
    func tableView(forKey key: String) -> [String]? {
        // Check for valid data
        if let binaryData = data(forKey: key) {
            // Is that Data a UITableView
            if let table = NSKeyedUnarchiver.unarchiveObject(with: binaryData) as? [String] {
                // Return the UITableView
                return table
            }
        }
        // If for some reason we didn't mak it to our table return, then something is wrong with this data
        return nil
    }
    
    // Save a UIColor as a data object
    func set(color: UIColor, forKey key: String) {
        
        // Convert the UICOlor object into a Data Object by Archiving
        let binaryData = NSKeyedArchiver.archivedData(withRootObject: color)
        
        // Save Binary Data to UserDefaults
        self.set(binaryData, forKey: key)
    }
    
    // Get UIColor from the saved Defaults with a key
    func color(forKey key: String) -> UIColor? {
        // Check for valid data
        if let binaryData = data(forKey: key) {
            // Is that Data a UIColor
            if let color = NSKeyedUnarchiver.unarchiveObject(with: binaryData) as? UIColor {
                // Return the UIcolor
                return color
            }
        }
        // If for some reason we didn't make it to our color return, then something is wrong with this data
        return nil
    }
}
