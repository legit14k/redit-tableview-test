//
//  ThemeViewController.swift
//  GrimbergJasonCE09
//
//  Created by Jason Grimberg on 9/21/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import UIKit

class ThemeViewController: UIViewController {

    var currentSelection = 1
    
    // Label Outlets
    @IBOutlet weak var labelInfo: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var viewLabel: UILabel!
    @IBOutlet weak var preView: UIView!
    
    
    // Slider Outlets
    @IBOutlet weak var redSlider: UISlider!
    @IBOutlet weak var greenSlider: UISlider!
    @IBOutlet weak var blueSlider: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set default label text
        labelInfo?.text = "Title editing"
        
        // Set all sliders to a default
        for slider in [redSlider, greenSlider, blueSlider] {
            slider?.value = 0
        }
        
        callBackColors()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        // Save the current color via our extension method
        UserDefaults.standard.set(color: titleLabel.textColor!, forKey: "titleColor")
        UserDefaults.standard.set(color: authorLabel.textColor!, forKey: "authorColor")
        UserDefaults.standard.set(color: preView.backgroundColor!, forKey: "viewColor")
        
    }
 
    
    @IBAction func colorTapped(_ sender: UIButton) {
        
        // Set the Tag
        let i = sender.tag
        // Switch the title for what is pressed
        switch i {
        case 1:
            currentSelection = 1
            labelInfo?.text = "Title editing"
            // Set all sliders to a default
            for slider in [redSlider, greenSlider, blueSlider] {
                slider?.value = 0
            }
        case 2:
            currentSelection = 2
            labelInfo?.text = "Author editing"
            // Set all sliders to a default
            for slider in [redSlider, greenSlider, blueSlider] {
                slider?.value = 0
            }
        case 3:
            currentSelection = 3
            labelInfo?.text = "View editing"
            // Set all sliders to a default
            for slider in [redSlider, greenSlider, blueSlider] {
                slider?.value = 0
            }
        default:
            print("Something went wrong")
        }
    }
    
    // Check the slider when it is changed and save the changes
    @IBAction func sliderDidChange(_ sender: UISlider) {
        
        switch currentSelection {
        case 1:
            // Update the current selection's color
            titleLabel.textColor = UIColor(displayP3Red: CGFloat(redSlider.value), green: CGFloat(greenSlider.value),
                                          blue: CGFloat(blueSlider.value), alpha: 1)
        case 2:
            // Update the current selection's color
            authorLabel.textColor = UIColor(displayP3Red: CGFloat(redSlider.value), green: CGFloat(greenSlider.value),
                                           blue: CGFloat(blueSlider.value), alpha: 1)
        case 3:
            // Update the current selection's color
            preView.backgroundColor = UIColor(displayP3Red: CGFloat(redSlider.value), green: CGFloat(greenSlider.value),
                                           blue: CGFloat(blueSlider.value), alpha: 1)
        default:
            print("Something went wrong...")
        }
    }
    
    @IBAction func resetTapped(_ sender: UIBarButtonItem) {
        
        titleLabel.textColor = UIColor(displayP3Red: CGFloat(0), green: CGFloat(0), blue: CGFloat(0), alpha: 1)
        authorLabel.textColor = UIColor(displayP3Red: CGFloat(0), green: CGFloat(0), blue: CGFloat(0), alpha: 1)
        preView.backgroundColor = UIColor(displayP3Red: CGFloat(1), green: CGFloat(1), blue: CGFloat(1), alpha: 1)
        
        currentSelection = 1
        labelInfo?.text = "Title editing"
        // Set all sliders to a default
        for slider in [redSlider, greenSlider, blueSlider] {
            slider?.value = 0
        }
    }
    
    func callBackColors() {
        // Load color from UserDefaults
        if let viewColorDefault = UserDefaults.standard.color(forKey: "viewColor"){
            preView.backgroundColor = viewColorDefault
        }
        if let titleColorDefault = UserDefaults.standard.color(forKey: "titleColor"){
            titleLabel.textColor = titleColorDefault
        }
        if let authorColorDefault = UserDefaults.standard.color(forKey: "authorColor"){
            authorLabel.textColor = authorColorDefault
        }
    }
    
}
